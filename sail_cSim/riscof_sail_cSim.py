import os
import re
import shutil
import subprocess
import shlex
import logging
import random
import string
from string import Template

import riscof.utils as utils
from riscof.pluginTemplate import pluginTemplate
import riscof.constants as constants
from riscv_isac.isac import isac

logger = logging.getLogger()

map = {
    'rv32i': 'rv32i',
    'rv32im': 'rv32im',
    'rv32ic': 'rv32ic',
    'rv32ia': 'rv32ia',
    'rv32imc': 'rv32imc',
    'rv64i': 'rv64i',
    'rv64imc': 'rv64imc',
    'rv64im': 'rv64im',
    'rv64ic': 'rv64ic',
    'rv64ia': 'rv64ia'

}

#TODO: change the following to point to the c_emulator directory of Sail
# Should include the last slash. Can be empty if spike is available on the $PATH
sail_emulator_path = '/scratch/git-repo/github/sail-riscv/c_emulator/'

class sail_cSim(pluginTemplate):
    __model__ = "sail_c_simulator"
    __version__ = "0.5.0"

    def __init__(self, *args, **kwargs):
        sclass = super().__init__(*args, **kwargs)

        config = kwargs.get('config')
#        if config is None:
#            print("Please enter input file paths in configuration.")
#            raise SystemExit
#        else:
#            self.isa_spec = os.path.abspath(config['ispec'])
#            self.platform_spec = os.path.abspath(config['pspec'])
        self.pluginpath = os.path.abspath(config['pluginpath'])

        return sclass

    def initialise(self, suite, work_dir, compliance_env):
        self.suite = suite
        self.work_dir = work_dir
        self.objdump_cmd = 'riscv{1}-unknown-elf-objdump -D {0} > {2};'
        self.compile_cmd = 'riscv{1}-unknown-elf-gcc -march={0} \
         -static -mcmodel=medany -fvisibility=hidden -nostdlib -nostartfiles\
         -T '+self.pluginpath+'/env/link.ld\
         -I '+self.pluginpath+'/env/\
         -I ' + compliance_env

    def build(self, isa_yaml, platform_yaml):
        ispec = utils.load_yaml(isa_yaml)['hart0']
        self.xlen = ('64' if 64 in ispec['supported_xlen'] else '32')
        self.isa = 'rv' + self.xlen
        self.compile_cmd = self.compile_cmd+' -mabi='+('lp64 ' if 64 in ispec['supported_xlen'] else 'ilp32 ')
        if "I" in ispec["ISA"]:
            self.isa += 'i'
        if "M" in ispec["ISA"]:
            self.isa += 'm'
        if "C" in ispec["ISA"]:
            self.isa += 'c'

    def runTests(self, testList,cgf_file=None):
        make = utils.makeUtil(makefilePath=os.path.join(self.work_dir, "Makefile." + self.name[:-1]))
        make.makeCommand = 'make -j8'
        #make.makeCommand = 'pmake -j 8'
        for file in testList:
            testentry = testList[file]
            test = testentry['test_path']
            test_dir = testentry['work_dir']
            test_name = test.rsplit('/',1)[1][:-2]

            elf = test_name + '.elf'

            execute = "cd "+testentry['work_dir']+";"

            cmd = self.compile_cmd.format(testentry['isa'].lower(), self.xlen) + ' ' + test + ' -o ' + elf
            compile_cmd = cmd + ' -D' + " -D".join(testentry['macros'])
            execute+=compile_cmd+";"

            execute += self.objdump_cmd.format(elf, self.xlen, test_name+'.disass')
            sig_file = os.path.join(test_dir, self.name[:-1] + ".signature")

            #sig_path = test.replace('src','references')

            execute += sail_emulator_path + 'riscv_sim_RV{0} --test-signature={1} {2} > {3}.log 2>&1;'.format(self.xlen, sig_file, elf, test_name)

            cov_str = ' '
            for label in testentry['coverage_labels']:
                cov_str+=' -l '+label

            if cgf_file is not None:
                coverage_cmd = 'riscv_isac --verbose info coverage -d -t {0}.log --mode c_sail -o coverage.rpt  --sig-label begin_signature  end_signature --test-label rvtest_code_begin rvtest_code_end -e {0}.elf -c {1} -x{2} {3};'.format(test_name, cgf_file, self.xlen, cov_str)
            else:
                coverage_cmd = ''


            execute+=coverage_cmd

            make.add_target(execute)
        make.execute_all(self.work_dir)
